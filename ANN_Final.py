from matplotlib import pyplot as plt
import csv
import math
import random
from numpy import loadtxt
import numpy as np
from idpso import idpso
from sklearn.metrics import mean_squared_error, r2_score
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier ,KerasRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from keras.constraints import maxnorm
import pandas as pd
from keras.constraints import maxnorm
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RandomizedSearchCV


def create_model():
  print("1- ANN Regression")

  #model initializing
  model = Sequential()
  model.add(Dense(10, input_dim= 13 , activation='sigmoid'))

  model.add(Dense(10, activation = 'sigmoid'))

  model.add(Dense(1, activation = 'sigmoid'))
  model.compile(loss='mean_squared_error', optimizer='RMSprop' , metrics=['accuracy'])


  # y1_predict= model.predict(x1_test)

  return model

model = create_model()

#macros
#temperature
Tr = 295
I =  6
V = 1 
alpha = 0.1
Cp = 0.1*10**-6
h = 50 
As = 60*10**-4
m = 50*10**-3
C = 900
# t = 400 #t can be set to be a changable random value   # t = random.randint(300,10001) 
#availability
MTTFtpo = 27800
MTTR = 8
Ea = 0.642
k = 0.0000863
TPo = 293.15

#initializing
Temps_K = []
availabilities = []
Temps_test = []
vcpu=[]
cpu_data = []
mem_data = []
sto_data = []
temperature_data = []
step_predict = []
#data containers
df = pd.read_csv("res/vnfs.csv")
array_m = df.values

Lentgh = len(array_m[: , 11:12]) #get lenght of cpu usage coulmn
X1 = np.ones((Lentgh,3),dtype=float) #create anew numpy array to store temperature and availability values

#calculating availability
for cpur,time,i in zip(array_m[: , 11:12],array_m[: , 14:],range(0,Lentgh)): #loops for cpu, time coulmns 
    #temperature
    TP = Tr + (((I*V)+(alpha*alpha*V**2*cpur))/h*As)*(1- math.exp(-(((h*As*3600*abs(time))/(m*C))))) #variable is cpur

    #availability
    MTTFtp = MTTFtpo * math.exp((Ea/k)*(1/TP-1/TPo)) #variable is TP
    availability = MTTFtp / (MTTFtp+MTTR) #variable is MTTFtpo
    #store data
    X1[i][0] = abs(time) #add time coulmn but after absolute calculation *** to relate line 59
    X1[i][1] = TP
    X1[i][2] = availability
    Temps_K.append(TP)
    availabilities.append(availability)
yreal=availabilities

#merge two datasets 

array_m = np.delete(array_m, np.s_[14:], axis=1) #delete time coulmn to be replaced by X1[][0] after absolute *** to relate line 52

array_m = np.append(array_m, X1, axis=1) # merge

#Train and Test datasets
Y = array_m[:, 16:] #split the target values alone *** availabilities values
TEMP = array_m[:, 15:16] #split temperature values alone ***
# array_m = np.delete(array_m, np.s_[11:12], axis=1) #droping cpu
array_m = np.delete(array_m, np.s_[16:], axis=1) #delete availabilities *** after split it in line 64
array_m = np.delete(array_m, np.s_[0:3], axis=1) #drop id, type coulmn
X = array_m # assign all feature remaining to variable X

#create dataframes 
X = pd.DataFrame(X)
Y = pd.DataFrame(Y)
TEMP = pd.DataFrame(TEMP)

#train_test_split
x1_train , x1_test , y1_train , y1_test =  train_test_split(X, Y, test_size=0.2, random_state=0)



Y = list(Y[0])
model.fit(x1_train,y1_train,epochs=100)

predictions = model.predict(x1_test)
predictions=predictions.reshape(len(predictions),)
y1_test =list(y1_test.iloc[:,0])


from funcs import LossFunctions
lossFuns = LossFunctions()



from idpso import idpso
from pyswarm import pso
from keras.wrappers.scikit_learn import KerasClassifier ,KerasRegressor


from sklearn.model_selection import GridSearchCV
# model = KerasRegressor(build_fn=create_model, input_dim=13, nb_epoch=100, batch_size=10, verbose=0)


# def fitness(x):
#     print([x[0]])
#     clf = GridSearchCV(model,
#                           param_grid={}, verbose=2)
#     clf.fit(x1_train, y1_train)
#     a=clf.predict(x1_train)
#     mse = np.sqrt(lossFuns.MSE(y1_train, a))
#     return  mse
    
# lb = [1, 0.0001]
# ub = [1000, 3.0]
# xopt, fopt = pso(fitness, lb, ub, swarmsize=20, maxiter=5)
# pso_clf = GridSearchCV(create_model, cv=10, param_grid={"C": [xopt[0]], "gamma": [xopt[1]]}, verbose=2)
# pso_clf = GridSearchCV(create_model, cv=10, param_grid={"C": [481.6231902676099], "gamma": [3.0]}, verbose=2)
# pso_clf = GridSearchCV(create_model, cv=5, param_grid={"C": [xopt[0], 1e1, 1e2, 1e3],"gamma": np.logspace(-2, 2, 5)}, verbose=2)
# # clf = svm.SVR(kernel='rbf', C=xopt[0], gamma=xopt[1])
# pso_clf.fit(x1_test, y1_test)
# pso_y_predict = pso_clf.predict(x1_test)






# create model
model = KerasRegressor(build_fn=create_model, epochs=100, batch_size=10, verbose=0)
# define the grid search parameters
neurons = [1, 5, 10, 15, 20, 25, 30]
layers = [1,2,3,4]
optimizer = ['SGD', 'RMSprop', 'Adagrad', 'Adadelta', 'Adam', 'Adamax', 'Nadam']
activation = ['softmax', 'softplus', 'softsign', 'relu', 'tanh', 'sigmoid', 'hard_sigmoid', 'linear']


#input_dim = [x1_train.shape[1]]
param_grid = dict(neurons=neurons, layers = layers,optimizer = optimizer , activation = activation)
grid =  RandomizedSearchCV(estimator=model, param_distributions=param_grid, n_jobs=-1)
grid_result = grid.fit(x1_train, y1_train)
# summarize results
print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))
# def fitness(x):
#     print([x[0]])
#     clf = GridSearchCV(model,
#                           param_grid={}, verbose=2)
#     clf.fit(x1_train, y1_train)
#     a=clf.predict(x1_train)
#     mse = np.sqrt(lossFuns.MSE(y1_train, a))
#     return  mse
    
# lb = [1, 0.0001]
# ub = [1000, 3.0]
# xopt, fopt = idpso(fitness, lb, ub, swarmsize=20, maxiter=5)


# from idpso import idpso

# lb = [1, 0.0001]
# ub = [1000, 3.0]
# xopt, fopt = idpso(fitness, lb, ub, swarmsize=20, maxiter=5)
# idpso_clf = GridSearchCV(create_model, cv=10, param_grid={"C": [xopt[0]], "gamma": [xopt[1]]}, verbose=2)
# idpso_clf = GridSearchCV(create_model, cv=10, param_grid={"C": [481.6231902676099], "gamma": [3.0]}, verbose=2)
# idpso_clf = GridSearchCV(create_model, cv=5, param_grid={"C": [xopt[0], 1e1, 1e2, 1e3],"gamma": np.logspace(-2, 2, 5)}, verbose=2)
# # clf = svm.SVR(kernel='rbf', C=xopt[0], gamma=xopt[1])
# idpso_clf.fit(x1_test, y1_test)
# idpso_y_predict = idps_clf.predict(x1_test)










## error function
pe=lossFuns.PE(predict=y1_test,target=predictions)
ape=lossFuns.APE(predict=y1_test,target=predictions)
mse=lossFuns.MSE(predict=y1_test,target=predictions)
mae=lossFuns.MAE(predict=y1_test,target=predictions)
mape=lossFuns.MAPE(predict=y1_test,target=predictions)
arv=lossFuns.ARV(predict=y1_test,target=predictions)
uts=lossFuns.UTS(predict=y1_test,target=predictions)
rmse=lossFuns.RMSE(predict=y1_test,target=predictions)
pocid=lossFuns.POCID(predict=y1_test,target=predictions)
fitness=lossFuns.FITNESS(predict=y1_test,target=predictions)
nrmse=lossFuns.NRMSE(predict=y1_test,target=predictions)
err=lossFuns.ERR(predict=y1_test,target=predictions)
accuracy=lossFuns.accuracy(predict=y1_test,target=predictions)
ia= lossFuns.IA(predict=y1_test,target=predictions)


print('PE: %.4f ' % pe)
print('APE: %.4f ' % ape)
print('MSE: %.4f ' % mse)
print('MAE: %.4f ' % mae)
print('MAPE: %.4f ' % mape)
print('ARV: %.4f ' % arv)
print('UTS: %.4f ' % uts)
print('RMSE: %.4f ' % rmse)
print('POCID: %.4f ' % pocid)
print('FITNESS: %.4f ' % fitness)
print('NRMSE: %.4f ' % nrmse)
print('IA: %.4f ' % ia)




print('\n\n\n\nPSO\n')

## error function
pe=lossFuns.PE(predict=y1_test,target=pso_y_predict)
ape=lossFuns.APE(predict=y1_test,target=pso_y_predict)
mse=lossFuns.MSE(predict=y1_test,target=pso_y_predict)
mae=lossFuns.MAE(predict=y1_test,target=pso_y_predict)
mape=lossFuns.MAPE(predict=y1_test,target=pso_y_predict)
arv=lossFuns.ARV(predict=y1_test,target=pso_y_predict)
uts=lossFuns.UTS(predict=y1_test,target=pso_y_predict)
rmse=lossFuns.RMSE(predict=y1_test,target=pso_y_predict)
pocid=lossFuns.POCID(predict=y1_test,target=pso_y_predict)
fitness=lossFuns.FITNESS(predict=y1_test,target=pso_y_predict)
nrmse=lossFuns.NRMSE(predict=y1_test,target=pso_y_predict)
err=lossFuns.ERR(predict=y1_test,target=pso_y_predict)
accuracy=lossFuns.accuracy(predict=y1_test,target=pso_y_predict)
ia= lossFuns.IA(predict=y1_test,target=pso_y_predict)


print('PE: %.4f ' % pe)
print('APE: %.4f ' % ape)
print('MSE: %.4f ' % mse)
print('MAE: %.4f ' % mae)
print('MAPE: %.4f ' % mape)
print('ARV: %.4f ' % arv)
print('UTS: %.4f ' % uts)
print('RMSE: %.4f ' % rmse)
print('POCID: %.4f ' % pocid)
print('FITNESS: %.4f ' % fitness)
print('NRMSE: %.4f ' % nrmse)
print('IA: %.4f ' % ia)






print('\n\n\n\nIDPSO\n')

## error function
pe=lossFuns.PE(predict=y1_test,target=idpso_y_predict)
ape=lossFuns.APE(predict=y1_test,target=idpso_y_predict)
mse=lossFuns.MSE(predict=y1_test,target=idpso_y_predict)
mae=lossFuns.MAE(predict=y1_test,target=idpso_y_predict)
mape=lossFuns.MAPE(predict=y1_test,target=idpso_y_predict)
arv=lossFuns.ARV(predict=y1_test,target=idpso_y_predict)
uts=lossFuns.UTS(predict=y1_test,target=idpso_y_predict)
rmse=lossFuns.RMSE(predict=y1_test,target=idpso_y_predict)
pocid=lossFuns.POCID(predict=y1_test,target=idpso_y_predict)
fitness=lossFuns.FITNESS(predict=y1_test,target=pidso_y_predict)
nrmse=lossFuns.NRMSE(predict=y1_test,target=idpso_y_predict)
err=lossFuns.ERR(predict=y1_test,target=idpso_y_predict)
accuracy=lossFuns.accuracy(predict=y1_test,target=idpso_y_predict)
ia= lossFuns.IA(predict=y1_test,target=idpso_y_predict)


print('PE: %.4f ' % pe)
print('APE: %.4f ' % ape)
print('MSE: %.4f ' % mse)
print('MAE: %.4f ' % mae)
print('MAPE: %.4f ' % mape)
print('ARV: %.4f ' % arv)
print('UTS: %.4f ' % uts)
print('RMSE: %.4f ' % rmse)
print('POCID: %.4f ' % pocid)
print('FITNESS: %.4f ' % fitness)
print('NRMSE: %.4f ' % nrmse)
print('IA: %.4f ' % ia)





# predictions = model.predict(x1_test)
# predictions = predictions.reshape(len(predictions.tolist()),)
# predictions = predictions.tolist()
# # y1_test=y1_test[0]
# # Cumulative Distribution Function of the error   
error_v=[0,2,4,6,8,10,12,14]
pro1=np.zeros(len(error_v))
pro2=np.zeros(len(error_v))
pro3=np.zeros(len(error_v))
for k in range(len(error_v)):
    num=0.0
    for i in range(len(y1_test)):
        val1=np.abs(y1_test[i]-predictions[i])/y1_test[i]
        if(val1<error_v[k]/100.0):
            num+=1
    pro1[k]=num/float(len(y1_test))
    
    for i in range(len(y1_test)):
        val1=np.abs(y1_test[i]-pso_y_predict[i])/y1_test[i]
        if(val1<error_v[k]/100.0):
            num+=1
    pro2[k]=num/float(len(y1_test))
    
    for i in range(len(y1_test)):
        val1=np.abs(y1_test[i]-idpso_y_predict[i])/y1_test[i]
        if(val1<error_v[k]/100.0):
            num+=1
    pro3[k]=num/float(len(y1_test))
    
    
    
    
    
    
fig, ax = plt.subplots()
plt.plot(error_v, pro1, label='ANN', linewidth=1)
plt.plot(error_v, pro2, label='PSO', linewidth=1)
plt.plot(error_v, pro3, label='IDPSO', linewidth=1)
plt.xlabel('Error[%]')
plt.ylabel('Probability')
plt.title('Cumulative Distribution Function of the error')
ax.legend(loc='lower right')
plt.show()






plt.plot(yreal, 'g', label='Real Data', linewidth=1)
plt.grid()
plt.title("only real data")
plt.legend()
plt.show()
plt.close()


all_predicted_y = model.predict(X)






plt.plot(yreal, 'g', label='Real Data', linewidth=1)
plt.plot(all_predicted_y, 'r', label='predict Data', linewidth=1)

plt.grid()
plt.title("predictions & real")
plt.legend()
plt.show()
plt.close()










pso_y_predict = pso_clf.predict(X)

plt.plot(yreal, 'g', label='Real Data', linewidth=1)
plt.plot(pso_y_predict, 'r', label='pso', linewidth=1)

plt.grid()
plt.title("pos & real")
plt.legend()
plt.show()
plt.close()





idpso_y_predict = idpso_clf.predict(X)

plt.plot(yreal, 'g', label='Real Data', linewidth=1)
plt.plot(idpso_y_predict, 'r', label='idpso', linewidth=1)

plt.grid()
plt.title("idpos & real")
plt.legend()
plt.show()
plt.close()

