import numpy as np
import pandas as pd
from sklearn import datasets, linear_model
from sklearn.preprocessing import StandardScaler
from sklearn.cross_validation import train_test_split

def LR_model_fun(x1_train , y1_train , x1_test):
	print("1- Linear Regression")

	LR_mod = linear_model.LinearRegression()

	LR_mod.fit(x1_train, y1_train)

	y1_predict = LR_mod.predict(x1_test)

	return y1_predict, LR_mod

