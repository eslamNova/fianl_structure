import numpy as np

class LossFunctions(object):
    
    ############
    #Constractor
    ############
    def __init__(self,target=None,predict=None):
        """        
        Args:
            target: array of the real values.(optianal)
            predict: array of the predict values.(optianal)
        
        Returns:
            Nothing
        
        Raises:
            Nothing
        """
        self.target = target
        self.predict = predict
        
    ##############
    #Predict Error
    #############
    def PE(self,target=None,predict=None):
        """        
        Args:
            target: array of the real values.(optianal)
            predict: array of the predict values.(optianal)
        
        Returns:
            Float number of Predcited Error 
        Raises:
            ValueError:when there's no data passed or saved in the class
        """
        
        ##EQUATION##
        ''''
        sum(target_j -predict_j)/N
        '''
        if not ((target is None) and (predict is None)): #Check if new data is passed or not if data is passed, then save values in the class members self.target ,self.predict 
            self.target = target #assign self.target to target to save the values
            self.predict = predict#assign self.predict to predict to save the values
        assert (self.target is not np.ndarray) and (self.predict is not np.ndarray),'Some values is None. Please recall function with real values' #chech if there's data in the class member. if there is no data will through error
        
        n = len(self.predict) #save length of the array in variable to use it later
        eq_result = np.sum(np.array(self.target)- np.array(self.predict)) #calculate the defference between real value and predicted value
        return float(eq_result) / float(n) # calculat the mean of the defference between real value and predicted value and return the value
    
    ######################
    #Average Predict Error
    ######################
    def APE(self,target=None,predict=None):
        """        
        Args:
            target: array of the real values.(optianal)
            predict: array of the predict values.(optianal)
        
        Returns:
            Float number of Average Predcited Error 
        Raises:
            ValueError:when there's no data passed or saved in the class
        """
         ##EQUATION##
        ''''
        sum(PE_i(target_j,predict_j))/100.0
        '''
        
        ######################################
        ####PLEASE CHECK THIS FUNCTION########
        ######################################
        
        if not ((target is None) and (predict is None)):#Check if new data is passed or not if data is passed, then save values in the class members self.target ,self.predict 
            self.target = target #assign self.target to target to save the values
            self.predict = predict#assign self.predict to predict to save the values
        assert (self.target is not np.ndarray) and (self.predict is not np.ndarray),'Some values is None. Please recall function with real values' #chech if there's data in the class member. if there is no data will through error
        
        Sum = 0 # initailize Sum variabe to save PE values 
        for i in range (100):
            Sum += self.PE(self.target,self.predict)#calculate PE and add value to Sum variable
        return float(Sum)/100.0 # calculat the mean of all defference between real value and predicted value and return the value


    ##################
    #Mean Square Error
    ##################
    def MSE(self,target=None,predict=None):
        """        
        Args:
            target: array of the real values.(optianal)
            predict: array of the predict values.(optianal)
        
        Returns:
            Float number of Mean Square Error
        Raises:
            ValueError:when there's no data passed or saved in the class
        """
        
        ##EQUATION##
        ''''
        sum((target_j-predict_j)^2)/N
        '''
        if not ((target is None) and (predict is None)):#Check if new data is passed or not if data is passed, then save values in the class members self.target ,self.predict 
            self.target = target #assign self.target to target to save the values
            self.predict = predict#assign self.predict to predict to save the values
            
        assert (self.target is not np.ndarray) and (self.predict is not np.ndarray),'Some values is None. Please recall function with real values' #chech if there's data in the class member. if there is no data will through error

        n = len(self.predict)#save length of the array in variable to use it later
        Sum =  np.sum((np.array(self.target)-np.array(self.predict))**2) #calculate the defference between real value and predicted value squared 

        return float(Sum)/float(n) # calculat the mean of the defference between real value and predicted squared value and return the value 
    

    #####
    #MAE
    ####
    def MAE(self,target=None,predict=None):
        """        
        Args:
            target: array of the real values.(optianal)
            predict: array of the predict values.(optianal)
        
        Returns:
            Float number of MAE
        Raises:
            ValueError:when there's no data passed or saved in the class
        """
        ##EQUATION##
        ''''
        sum(|(predict_j-target_j)/targert_j|)/N
        '''
        if not ((target is None) and (predict is None)):#Check if new data is passed or not if data is passed, then save values in the class members self.target ,self.predict 
            self.target = target #assign self.target to target to save the values
            self.predict = predict#assign self.predict to predict to save the values
        assert (self.target is not np.ndarray) and (self.predict is not np.ndarray),'Some values is None. Please recall function with real values' #chech if there's data in the class member. if there is no data will through error
        
        Sum=0 # initailize Sum variabe to save PE values 
        n=len(self.predict)#save length of the array in variable to use it later
       
        for i in range(n):
            percentageerror = abs((self.predict[i]-self.target[i])/self.target[i])   #calculate the defference between real value and predicted value divied by predicted value        
            Sum+= percentageerror #calculate the defference between real value and predicted value squared divied by predicted value and added to Sum variable
        return float(Sum)/float(n) # calculat the mean of the defference between real value and predicted value divied by predicted value and return the value 
    

    ################################
    #Mean Absolute Percentage Error
    ###############################
    def MAPE(self,target=None,predict=None):
        """        
        Args:
            target: array of the real values.(optianal)
            predict: array of the predict values.(optianal)
        
        Returns:
            Float number of Mean Absolute Percentage Error
        Raises:
            ValueError:when there's no data passed or saved in the class
        """
        ##EQUATION##
        ''''
        sum(|(target_j-predict_j)/target_j|)X(100/N)
        '''
        
        if not ((target is None) and (predict is None)):#Check if new data is passed or not if data is passed, then save values in the class members self.target ,self.predict 
            self.target = target #assign self.target to target to save the values
            self.predict = predict#assign self.predict to predict to save the values
        assert (self.target is not np.ndarray) and (self.predict is not np.ndarray),'Some values is None. Please recall function with real values' #chech if there's data in the class member. if there is no data will through error
       
        Sum=0 # initailize Sum variabe to save PE values 
        n=len(self.predict)#save length of the array in variable to use it later
        
        for i in range(n):
          if(abs(self.target[i])>0.00001):#check if Absult value of target is greater than 0.000001
              Sum+=abs((self.target[i]-self.predict[i])/float(self.target[i])) #calculate the absult value of defference between real value and predicted value divied by predicted value        
        return float(Sum)/float(n)*100 # calculat the mean of Sum multipled by 100    




      #########################
     #Average Relative Variance
    ##########################
    def ARV(self,target=None,predict=None):
        """        
        Args:
            target: array of the real values.(optianal)
            predict: array of the predict values.(optianal)
        
        Returns:
             Float number of Average Relative Variance
        Raises:
            ValueError:when there's no data passed or saved in the class
        """
        ##EQUATION##
        ''''
        sum((predict_j-target_j)^2) / sum((predict_j-mean_target)^2)
        '''
        
        if not ((target is None) and (predict is None)):#Check if new data is passed or not if data is passed, then save values in the class members self.target ,self.predict 
            self.target = target #assign self.target to target to save the values
            self.predict = predict#assign self.predict to predict to save the values
        assert (self.target is not np.ndarray) and (self.predict is not np.ndarray),'Some values is None. Please recall function with real values' #chech if there's data in the class member. if there is no data will through error
       
        n = len(self.target)#save length of the array in variable to use it later
        mean_target = np.sum(self.target)/float(n)#calculate the mean of target valuese
        top = np.sum((np.array(self.predict)-np.array(self.target))**2) #calculate the top part to the equation by take the sam of abs of the defference between real value and predicted value divied by predicted value squared
        down = np.sum((self.predict-mean_target)**2) #calculate the down part to the equation by take the sam of abs of the defference between predict value and mean target value squared

        return float(top)/float(down)
    
    
    
    ''' OLD METHOD'''
#    def ARV(self,target=None,predict=None):
#        """        
#        Args:
#            target: array of the real values.(optianal)
#            predict: array of the predict values.(optianal)
#        
#        Returns:
#             Float number of Average Relative Variance
#        Raises:
#            ValueError:when there's no data passed or saved in the class
#        """
#        
#        if not ((target is None) and (predict is None)):#Check if new data is passed or not if data is passed, then save values in the class members self.target ,self.predict 
#            self.target = target #assign self.target to target to save the values
#            self.predict = predict#assign self.predict to predict to save the values
#        assert (self.target is not np.ndarray) and (self.predict is not np.ndarray),'Some values is None. Please recall function with real values' #chech if there's data in the class member. if there is no data will through error
#       
#        n = len(self.predict)#save length of the array in variable to use it later
#        E=[] # list to save each Error value to use it later
#      
#        for i in range(n):
#          #E.append(abs(self.predict[i]-self.target[i]))
#          percentageerror = abs((self.predict[i] - self.target[i])/self.target[i]) #calculate the defference between real value and predicted value divied by predicted value        
#          #E.append(abs((self.predict[i] - self.target[i])/float(self.target[i])))
#          E.append(percentageerror)#add percentageerror to E list
#        E=np.array(E)#convert E List to numpy array
#        ME=np.mean(E)#calculate Mean of the Erorrs 
#        MS=np.var(E)#calculate Variance of the Errors
#        print(ME)
#        return float(MS)/float(ME*ME)

      #########################
     #U of Theil Statistics
    ##########################
    def UTS(self,target=None,predict=None):
        """        
        Args:
            target: array of the real values.(optianal)
            predict: array of the predict values.(optianal)
        
        Returns:
             Float number of U of Theil Statistics
        Raises:
            ValueError:when there's no data passed or saved in the class
        """
        ##EQUATION##
        ''''
        sum((target_j-predict_j)^2) / sum((predict_j-predict_j+1)^2)
        '''
        if not ((target is None) and (predict is None)):#Check if new data is passed or not if data is passed, then save values in the class members self.target ,self.predict 
            self.target = target #assign self.target to target to save the values
            self.predict = predict#assign self.predict to predict to save the values
        assert (self.target is not np.ndarray) and (self.predict is not np.ndarray),'Some values is None. Please recall function with real values' #chech if there's data in the class member. if there is no data will through error
       
        top = np.sum((np.array(self.target)-np.array(self.predict))**2)#calculate the top part of the equation which is the defference between real value and predicted value squared 
        down = np.sum((np.array(self.predict[:-1])-np.array(self.predict[1:]))**2)#calculate the down part of the equation which is the defference between predicted value and the next predicted value squared 
        return float(top)/float(down)#divid the top part on the down part and return the value

      #########################
     #Root Square Error
    ##########################
    def RMSE(self,target=None,predict=None):
        """        
        Args:
            target: array of the real values.(optianal)
            predict: array of the predict values.(optianal)
        
        Returns:
             Float number of Root Square Error
        Raises:
            ValueError:when there's no data passed or saved in the class
        """
        ##EQUATION##
        ''''
        squar_root(MSE(target_j,predict_j))
        '''
        if not ((target is None) and (predict is None)):#Check if new data is passed or not if data is passed, then save values in the class members self.target ,self.predict 
            self.target = target #assign self.target to target to save the values
            self.predict = predict#assign self.predict to predict to save the values
        assert (self.target is not np.ndarray) and (self.predict is not np.ndarray),'Some values is None. Please recall function with real values' #chech if there's data in the class member. if there is no data will through error
       
        return np.sqrt(self.MSE(self.target,self.predict))#calculate the mse and the calculate its root and return the value

     #########################
     #Prediction of Change in Direction
    ##########################
    def POCID(self,target=None,predict=None):
        """        
        Args:
            target: array of the real values.(optianal)
            predict: array of the predict values.(optianal)
        
        Returns:
             Float number of Prediction of Change in Direction
        Raises:
            ValueError:when there's no data passed or saved in the class
        """
        ##EQUATION##
        ''''
        x = 1 if (target_j - target_j-1) X (output_j - output_j-1)  > 0 
        x = 0 if (target_j - target_j-1) X (output_j - output_j-1)  < 0 
        sum(d_j)/n*100
        '''
        
        ##################################################
        ##################################################
        ############REturn list or one number ??????????#
        ##################################################
        if not ((target is None) and (predict is None)):#Check if new data is passed or not if data is passed, then save values in the class members self.target ,self.predict 
            self.target = target #assign self.target to target to save the values
            self.predict = predict#assign self.predict to predict to save the values
        assert (self.target is not np.ndarray) and (self.predict is not np.ndarray),'Some values is None. Please recall function with real values' #chech if there's data in the class member. if there is no data will through error
        
        
        '''
        TO RETURN LIST
        
        n = len(self.predict)
        l = []
        for i in range(n-1):
            v = (self.target[i+1]-self.target[i])*(self.predict[i+1]-self.predict[1])
            if  v>0
                l.append(1)
            else:
                l.append(0)
        return l
        '''
#        eq_result = np.sum(np.array(self.target[1:])-np.array(self.target[:-1]))*np.sum(np.array(self.predict[1:])-np.array(self.predict[:-1]))#sum of ((difference between target value and the next target value ) multipled by (difference between predicted value and the next pedicted value )) 
#        return 1 if eq_result > 0 else 0#chech if eq_reslut is greater than 0 return 1 else return 1
        n = len(self.predict)
        sum = 0
        for i in range(1,n):
            x = (self.target[i] - self.target[i-1])*(self.predict[i]-self.predict[i-1])
            if x>0:
                sum += 1
            else:
                sum +=0
        return float(sum)/float(n) *100
    

    
    
    
      #########################
     #FITNESS
    ##########################
    def FITNESS(self,target=None,predict=None):
        """        
        Args:
            target: array of the real values.(optianal)
            predict: array of the predict values.(optianal)
        
        Returns:
             Float number of FITNESS
        Raises:
            ValueError:when there's no data passed or saved in the class
        """
        ##EQUATION##
        ''''
        POCID / ( 1 + MSE + MAPE + UTS + ARV )
        '''
        if not ((target is None) and (predict is None)):#Check if new data is passed or not if data is passed, then save values in the class members self.target ,self.predict 
            self.target = target #assign self.target to target to save the values
            self.predict = predict#assign self.predict to predict to save the values
        assert (self.target is not np.ndarray) and (self.predict is not np.ndarray),'Some values is None. Please recall function with real values' #chech if there's data in the class member. if there is no data will through error
       
        top = self.POCID()#calculate the top part of equation by calling POCID method and it will use self.target and self.predict class members to get the values
        down = 1 + self.MSE() + self.MAPE() + self.UTS() + self.ARV()#calculate the down part of equation by sum of  calling 1  ,MSE,UTS,ARV methods and they will use self.target and self.predict class members to get the values

        return float(top)/float(down)#divid the top part on the down part and return the value

     #######
     #NRMSE
    ########
    def NRMSE(self,target=None,predict=None):
        """        
        Args:
            target: array of the real values.(optianal)
            predict: array of the predict values.(optianal)
        
        Returns:
             Float number of NRMSE
        Raises:
            ValueError:when there's no data passed or saved in the class
        """
        ##EQUATION##
        '''
        squar_root(sum((target_j-predict_j)**2)/N)/std(target)
        '''
        
        if not ((target is None) and (predict is None)):#Check if new data is passed or not if data is passed, then save values in the class members self.target ,self.predict 
            self.target = target #assign self.target to target to save the values
            self.predict = predict#assign self.predict to predict to save the values
        assert (self.target is not np.ndarray) and (self.predict is not np.ndarray),'Some values is None. Please recall function with real values' #chech if there's data in the class member. if there is no data will through error
       
        actuals = [] # list to save each raal value to use it later
        aggregatedError = 0 # variable to sum the defference between real value and predicted value squared
        n = 0
    
        for i in range(len(self.predict)):
            actuals.append(self.target[i])
            aggregatedError += (self.target[i] - self.predict[i])**2
            n += 1
    
        rmse = np.sqrt(aggregatedError / float(n))
        nrmse = rmse / np.std(actuals)
        return nrmse
    
    
    
     #########################
     #ERROR
    ##########################
    def ERR(self,target=None,predict=None):
        """        
        Args:
            target: array of the real values.(optianal)
            predict: array of the predict values.(optianal)
        
        Returns:
            Array of Errors
        Raises:
            ValueError:when there's no data passed or saved in the class
        """
        ##EQUATION##
        '''
        list of : |(predict_j-target_j)/target_j|
        '''
        if not ((target is None) and (predict is None)):#Check if new data is passed or not if data is passed, then save values in the class members self.target ,self.predict 
            self.target = target#assign self.target to target to save the values
            self.predict = predict#assign self.predict to predict to save the values
        assert (self.target is not np.ndarray) and (self.predict is not np.ndarray),'Some values is None. Please recall function with real values' #chech if there's data in the class member. if there is no data will through error
      
        n = len(self.predict)#save length of the array in variable to use it later
        errors = np.zeros(n, dtype=float)#create array with length of the pediect or target list
        for i in range(n):
            error = abs((self.predict[i]-self.target[i])/self.target[i])#calculate the defference between 
            errors.put(i, error)#save each error to errors array
        return errors#return the array of Errors

     #########################
     #accuracy
    ##########################   
    def accuracy(self,target=None,predict=None):
        """        
        Args:
            target: array of the real values.(optianal)
            predict: array of the predict values.(optianal)
        
        Returns:
            Array of Hits(accuracies)
        Raises:
            ValueError:when there's no data passed or saved in the class
        """
        ##EQUATION##
        '''
        list of : 1-|(predict_j-target_j)/target_j|
        '''
        
        if not ((target is None) and (predict is None)):#Check if new data is passed or not if data is passed, then save values in the class members self.target ,self.predict 
            self.target = target #assign self.target to target to save the values
            self.predict = predict#assign self.predict to predict to save the values
        assert (self.target is not np.ndarray) and (self.predict is not np.ndarray),'Some values is None. Please recall function with real values' #chech if there's data in the class member. if there is no data will through error
       
        n = len(self.predict)#save length of the array in variable to use it later
        hits = np.zeros(n, dtype=float)#create array with length of the pediect or target list
        for i in range(n):
            erro = abs((self.predict[i]-self.target[i])/self.target[i])#calculate the defference between real value and predicted value divied by predicted value        
            hit = 1-erro#calculate the hit
            hits.put(i, hit)#save each hit to hits array
        return hits#return the array of hits
    
    
    
    ##############################
    #Index of Agreement (IA)
    #############################
    
    def IA(self,target=None,predict=None):
        """        
        Args:
            target: array of the real values.(optianal)
            predict: array of the predict values.(optianal)
        
        Returns:
            Float number of Index of Agreement (IA)
        Raises:
            ValueError:when there's no data passed or saved in the class
        """
        ##EQUATION##
        '''
        1-  (sum(|predict_j-target_j|^2) / sum((|predict_j-mean_target|+|target_j-mean_target|)^2))
        '''
        if not ((target is None) and (predict is None)):#Check if new data is passed or not if data is passed, then save values in the class members self.target ,self.predict 
            self.target = target #assign self.target to target to save the values
            self.predict = predict#assign self.predict to predict to save the values
        assert (self.target is not np.ndarray) and (self.predict is not np.ndarray),'Some values is None. Please recall function with real values' #chech if there's data in the class member. if there is no data will through error
       
        n = len(self.target)#save length of the array in variable to use it later
        mean_target = np.sum(self.target)/float(n)#calculate the mean of target valuese
        top = np.sum(abs(self.predict-self.target)**2) #calculate the top part to the equation by take the sam of abs of the defference between real value and predicted value divied by predicted value squared
        down = np.sum((abs(self.predict-mean_target)+abs(self.target-mean_target))**2) #calculate the down part to the equation by take the sam of abs of the defference between predict value and mean target value added to the defference between target value and mean target  value all squared
        eq_result = 1-(float(top)/float(down))#finall equation result
        return eq_result
    
    
    
        
