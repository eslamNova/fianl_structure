from matplotlib import pyplot as plt
import csv
import math
import random
from numpy import loadtxt
import numpy as np
from sklearn import datasets, linear_model
from sklearn.preprocessing import StandardScaler
from sklearn.cross_validation import train_test_split
import pandas as pd

from helper import step_fun, test_cpu_data, complete_step_data
from LR_model import LR_model_fun
from SVM_model import SVM_model_fun
from all_metrics import calc_all_metrics
from ANN_model import ANN_model_fun

#macros
#temperature
Tr = 295
I =  6
V = 1 
alpha = 0.1
Cp = 0.1*10**-6
h = 50 
As = 60*10**-4
m = 50*10**-3
C = 900
# t = 400 #t can be set to be a changable random value   # t = random.randint(300,10001) 
#availability
MTTFtpo = 27800
MTTR = 8
Ea = 0.642
k = 0.0000863
TPo = 293.15

#initializing
Temps_K = []
availabilities = []
Temps_test = []
vcpu=[]
cpu_data = []
mem_data = []
sto_data = []
temperature_data = []
step_predict = []
def MAE(predict, test):
    Sum=0
    n=len(predict)
    for i in range(n):
        Sum+=abs(predict[i]-test[i])
    return float(Sum)/float(n)
# MSE function
def MSE(predict, test):
    Sum=0
    n=len(predict)
    for i in range(n):
        Sum+=(predict[i]-test[i])*(predict[i]-test[i])
    return float(Sum)/float(n)
# MAPE function
def MAPE(predict, test):
    Sum=0
    n=len(predict)
    for i in range(n):
        if(abs(test[i])>0.00001):
            Sum+=abs(predict[i]-test[i])/float(test[i])
    return float(Sum)/float(n)*100
# ARV function
def ARV(predict, test):
    n=len(predict)
    E=[]
    for i in range(n):
        E.append(abs(predict[i]-test[i]))
    E=np.array(E)
    ME=np.mean(E)
    MS=np.var(E)
    return float(MS)/float(ME*ME)
# RMSE function
def RMSE(predict, test):
    Sum = 0 
    n=len(predict)
    for i in range(n):
        Sum+= math.sqrt(MSE(predict,test))
    return float(Sum)/float(n)



#data containers
df = pd.read_csv("res/vnfs.csv")
array_m = df.values
Lentgh = len(array_m[: , 11:12]) #get lenght of cpu usage coulmn
X1 = np.ones((Lentgh,3),dtype=float) #create anew numpy array to store temperature and availability values

#calculating availability
for cpur,time,i in zip(array_m[: , 11:12],array_m[: , 14:],range(0,Lentgh)): #loops for cpu, time coulmns 
    
    #temperature
    TP = Tr + (((I*V)+(alpha*alpha*V**2*cpur))/h*As)*(1- math.exp(-(((h*As*3600*abs(time))/(m*C))))) #variable is cpur

    #availability
    MTTFtp = MTTFtpo * math.exp((Ea/k)*(1/TP-1/TPo)) #variable is TP
    availability = MTTFtp / (MTTFtp+MTTR) #variable is MTTFtpo
    
    #store data
    X1[i][0] = abs(time) #add time coulmn but after absolute calculation *** to relate line 59
    X1[i][1] = TP
    X1[i][2] = availability
    Temps_K.append(TP)
    availabilities.append(availability)

#merge two datasets 
array_m = np.delete(array_m, np.s_[14:], axis=1) #delete time coulmn to be replaced by X1[][0] after absolute *** to relate line 52
array_m = np.append(array_m, X1, axis=1) # merge


#Train and Test datasets
Y = array_m[:, 16:] #split the target values alone *** availabilities values
TEMP = array_m[:, 15:16] #split temperature values alone ***
# array_m = np.delete(array_m, np.s_[11:12], axis=1) #droping cpu
array_m = np.delete(array_m, np.s_[16:], axis=1) #delete availabilities *** after split it in line 64
array_m = np.delete(array_m, np.s_[0:3], axis=1) #drop id, type coulmn
X = array_m # assign all feature remaining to variable X

#create dataframes 
X = pd.DataFrame(X)
Y = pd.DataFrame(Y)
TEMP = pd.DataFrame(TEMP)

#train_test_split
x1_train , x1_test , y1_train , y1_test =  train_test_split(X, Y, test_size=0.2, random_state=0)

#getting temperature values related to test dataset
temp_val = y1_test.index.values # retreving index of all test examples
for te in temp_val: # find temperatue values with same index as test examples
    Temps_test.append(TEMP.iat[te,0])

#convert df to numpy array
x1_train = x1_train.values
x1_test	= x1_test.values
y1_train = y1_train.values
y1_test = y1_test.values
actual_data = np.append(y1_train,y1_test,axis=0) #sum all Y's to plot

# STEP FUNCTION
data_sample = x1_train[0:1 , : ]
test_sample = y1_train[0:1]
#to enter a key
#cpu_usage --> 8
#mem_usage --> 9
#sto_usage --> 10
#temperature --> 12

key = 12
iterations = 100
step = float(.0001)
step_data, new_data, new_data_name = step_fun(data_sample, key, iterations, step)
new_data = np.array(new_data).reshape(len(new_data), 1) #reshape # X axis
step_data = step_data.reshape(len(step_data), x1_train.shape[1]) #reshape 


#NEW FUNCTION
# dataset_length = input("enter the size of the new random dataset")
dataset_length = 1000 #or enter here to select size of new random dataset

#select coulmn limits
# #to enter a key
# #cpu_usage --> 8
# #mem_usage --> 9
# #sto_usage --> 10
# #temperature --> 12
cpu_c = 8
mem_c = 9
sto_c = 10 
temp_c = 12

r = random.randint(1,len(x1_train))
data_row = x1_train[r:r+1 , :]

data_sample_cpu = x1_train[: , cpu_c:cpu_c+1 ] #extract cpu coulmn
data_sample_cpu = np.array(data_sample_cpu)

data_sample_mem = x1_train[: , mem_c:mem_c+1 ] #extract mem coulmn
data_sample_mem = np.array(data_sample_mem)

data_sample_sto = x1_train[: , sto_c:sto_c+1 ] #extract sto coulmn
data_sample_sto = np.array(data_sample_sto)

data_sample_temp = x1_train[: , temp_c:temp_c+1 ] #extract temperatue coulmn
data_sample_temp = np.array(data_sample_temp)

for i in range(0,dataset_length):
    cpu_data.append(np.array(random.choice(data_sample_cpu)))
    mem_data.append(np.array(random.choice(data_sample_mem)))
    sto_data.append(np.array(random.choice(data_sample_sto)))
    temperature_data.append(np.array(random.choice(data_sample_temp)))
cpu_data = np.array(cpu_data)
mem_data = np.array(mem_data)
sto_data = np.array(sto_data)
temperature_data = np.array(temperature_data)

new_data, new_data_name = test_cpu_data(data_row, cpu_data, mem_data, sto_data, temperature_data)
new_data = np.array(new_data).reshape(len(new_data), x1_train.shape[1]) #reshape
#calculate all metrics
d1, d2, d3, d4, d5, d6 = calc_all_metrics(new_data)


# Feature Scaling
sc = StandardScaler()
x1_train = sc.fit_transform(x1_train)
x1_test = sc.transform(x1_test)
step_data = sc.transform(step_data)
new_data = sc.transform(new_data)
data_sample = sc.transform(data_sample)

#plot the relation between availabilities and temperature
fig1 = plt.figure(1)
ax1 = fig1.add_axes([.11,.11,0.8,0.8])
ax1.plot(Temps_K, availabilities)
ax1.set(xlim=(min(Temps_K),max(Temps_K)),ylim=(min(availabilities) , max(availabilities)),ylabel='availability',xlabel='temperature in Kelvin',title='availability & temperature')
ax1.get_xaxis().get_major_formatter().set_useOffset(False)
ax1.get_yaxis().get_major_formatter().set_useOffset(False)

#Machine learning
#LR
y1_predict, model = LR_model_fun(x1_train , y1_train , x1_test)
#predict data
training_data_prediction = model.predict(x1_train)

#predict NEW FUNCTION
NEW_FUNCTION_predicted = model.predict(new_data)

#SVM ***only for ploting "Compare all algorithms"
y1_predict_svm, model_svm = SVM_model_fun(x1_train , y1_train , x1_test)

#STEP DATA prediction AND FILE EXTRACTION
test_sample_n = np.repeat(test_sample, iterations, axis=0)
pre = []
step_time = []

with open('error_files/avail_LR_error_2.csv', 'w', newline='') as f1:
    w1 = csv.writer(f1)
    w1.writerow(["Step Ahead", "MSE", "MAE", "MAPE", "RMSE", "ARV"])
    for i in range(1,iterations):
        if (i % 5 == 0 or i == 1):
            step_data_sample = step_data[0:i]
            complete_data = complete_step_data(step_data_sample, data_sample, iterations-i)
            pre =  model.predict(complete_data)
            text = str(i)+ " Step Ahead"
            mse=MSE(pre,test_sample_n)
            mae=MAE(pre,test_sample_n)
            mape=MAPE(pre,test_sample_n)
            rmse=RMSE(pre,test_sample_n)
            arv=ARV(pre,test_sample_n)
            step_time.append(i)
            w1.writerow([text, mse, mae, mape, rmse, arv])

#error 
df2 = pd.read_csv("error_files/avail_LR_error_2.csv")
error_array = df2.values
error_array = np.delete(error_array, np.s_[0:1], axis=1)
#SELECT ERROR TO PLOT ERROR DIAGRAM WITH STEP TIME
# MSE >> 0
# MAE >> 1 
# MAPE >> 2
# RMSE >> 3
# ARV >> 4
error_text_list = ['MSE', 'MAE', 'MAPE', 'RMSE', 'ARV']
plot_error = 0
plot_error_2 = 1
error_data = error_array[:, plot_error:plot_error+1]
error_data_2 = error_array[:, plot_error_2:plot_error_2+1]

#ANN REGRESSION
model_ANN = ANN_model_fun(x1_train, y1_train, 'adam', 'sigmoid', 4, 20)
model_ANN.fit(x1_train, y1_train, epochs=500, batch_size=10, verbose=2)
y1_predict= model.predict(x1_test)

#plot results
fig4 = plt.figure(4)
ax4 = fig4.add_axes([.11,.11,0.8,0.8])
ax4.plot(Temps_test,y1_predict,'--r',label='predected data')
ax4.plot(Temps_test,y1_test,'-b',label='real data')
ax4.set(ylabel='availability', xlabel='temperature in Kelvin',title='ANN Regression - relation')
ax4.get_xaxis().get_major_formatter().set_useOffset(False)
ax4.get_yaxis().get_major_formatter().set_useOffset(False)
ax4.legend(loc='upper right')

#edit before drawing for scaling
ll = .1 * int(len(y1_predict))
predect_dr = y1_predict[0: int(ll)]
real_dr = y1_test[0: int(ll)]

fig44 = plt.figure(44)
ax44 = fig44.add_axes([.11,.11,0.8,0.8])
ax44.plot(predect_dr,'--r',label='predected data')
ax44.plot(real_dr,'-b',label='real data')
# use ylim=(min(availabilities)-.00000001,max(availabilities)+.00000001) to control y axis scale by increasing or decresing zeros
ax44.set(ylim=(min(availabilities)-.00000001,max(availabilities)+.00000001), ylabel='availability', title='ANN Regression - real VS predected')
ax44.get_xaxis().get_major_formatter().set_useOffset(False)
ax44.get_yaxis().get_major_formatter().set_useOffset(False)
ax44.legend(loc='upper right')

# pre1 =[]
# pre2 =[]

# with open('error_files/avail_ANN_1l_error_2.csv', 'w', newline='') as f1:
#     w1 = csv.writer(f1)
#     w1.writerow(["Step Ahead", "MSE", "MAE", "MAPE", "RMSE", "ARV"])
#     for i in range(1,iterations):
#         if (i % 5 == 0 or i == 1):
#             step_data_sample = step_data[0:i]
#             complete_data = complete_step_data(step_data_sample, data_sample, iterations-i)
#             pre1 =  ANN_model_1l.predict(complete_data)
#             text = str(i)+ " Step Ahead"
#             mse=MSE(pre1,test_sample_n)
#             mae=MAE(pre1,test_sample_n)
#             mape=MAPE(pre1,test_sample_n)
#             rmse=RMSE(pre1,test_sample_n)
#             arv=ARV(pre1,test_sample_n)
#             w1.writerow([text, mse, mae, mape, rmse, arv])
# with open('error_files/avail_ANN_2l_error_2.csv', 'w', newline='') as f1:
#     w1 = csv.writer(f1)
#     w1.writerow(["Step Ahead", "MSE", "MAE", "MAPE", "RMSE", "ARV"])
#     for i in range(1,iterations):
#         if (i % 5 == 0 or i == 1):
#             step_data_sample = step_data[0:i]
#             complete_data = complete_step_data(step_data_sample, data_sample, iterations-i)
#             pre2 =  ANN_model_2l.predict(complete_data)
#             text = str(i)+ " Step Ahead"
#             mse=MSE(pre2,test_sample_n)
#             mae=MAE(pre2,test_sample_n)
#             mape=MAPE(pre2,test_sample_n)
#             rmse=RMSE(pre2,test_sample_n)
#             arv=ARV(pre2,test_sample_n)
#             w1.writerow([text, mse, mae, mape, rmse, arv])

# #error 
# df_1l = pd.read_csv("error_files/avail_ANN_1l_error_2.csv")
# df_2l = pd.read_csv("error_files/avail_ANN_2l_error_2.csv")

# error_array_1l = df_1l.values

# error_array_2l = df_2l.values

# error_array_1l = np.delete(error_array_1l, np.s_[0:1], axis=1)
# error_array_2l = np.delete(error_array_2l, np.s_[0:1], axis=1)
# #SELECT ERROR TO PLOT ERROR DIAGRAM WITH STEP TIME
# # MSE >> 0
# # MAE >> 1 
# # MAPE >> 2
# # RMSE >> 3
# # ARV >> 4
# plot_error_ann = 0
# error_data_ann_1 = error_array_1l[:, plot_error_ann:plot_error_ann+1]
# error_data_ann_2 = error_array_2l[:, plot_error_ann:plot_error_ann+1]

# #compare ANN model layers
# fig27 = plt.figure(27)
# ax27 = fig27.add_axes([.11,.11,0.8,0.8])
# ax27.plot(error_data_ann_1,label='1 hidden layers')
# ax27.plot(error_data_ann_2,label='2 hidden layers')
# ax27.set(ylabel='learning error [' +error_text_list[plot_error_ann]+']' ,title='ANN COMPARE')
# ax27.get_xaxis().get_major_formatter().set_useOffset(False)
# ax27.get_yaxis().get_major_formatter().set_useOffset(False)
# ax27.legend(loc='upper right')


# #ALL ELLORs with multiple neurons
# data_ann = x1_test[0:20 , :]
# test_ann = y1_test[0:20]
# mse_ann = []
# mae_ann = []
# mape_ann = []
# rmse_ann = []
# arv_ann = []
# model_nodes = []
# for i in range (1,101,10):
#     ANN_model_loop = ANN_model_fun(x1_train,y1_train,1,i)
#     pre3 = ANN_model_loop.predict(data_ann)

#     mse=MSE(pre3,test_ann)
#     mse_ann.append(mse)
#     mae=MAE(pre3,test_ann)
#     mae_ann.append(mae)
#     mape=MAPE(pre3,test_ann)
#     mape_ann.append(mape)
#     rmse=RMSE(pre3,test_ann)
#     rmse_ann.append(rmse)
#     arv=ARV(pre3,test_ann)
#     arv_ann.append(arv)
#     model_nodes.append(i)

# #plot ALL ELLORs with multiple neurons
# fig29 = plt.figure(29)
# ax29 = fig29.add_axes([.11,.11,0.8,0.8])
# ax29.plot(mse_ann,model_nodes,label='MSE')
# ax29.plot(mae_ann,model_nodes,label='MAE')
# ax29.plot(mape_ann,model_nodes,label='MAPE')
# ax29.plot(rmse_ann,model_nodes,label='RMSE')
# ax29.plot(arv_ann,model_nodes,label='ARV')
# ax29.set(ylabel='ERRORs', xlabel='number of hidden nodes',title='ALL ERRORs PLOT')
# ax29.get_xaxis().get_major_formatter().set_useOffset(False)
# ax29.get_yaxis().get_major_formatter().set_useOffset(False)
# ax29.legend(loc='upper right')

# #plot neurons size with ERROr
# fig26 = plt.figure(26)
# ax26 = fig26.add_axes([.11,.11,0.8,0.8])
# for n,i in zip(model_nodes,range(0,len(mse_ann))):
#     ax26.plot(mse_ann[i],n,'^g',label=str(n)+'neurons')
# # ax26.plot(mse_ann,model_nodes,label='MSE')
# # ax26.plot(mae_ann,model_nodes,label='MAE')
# # ax26.plot(mape_ann,model_nodes,label='MAPE')
# # ax26.plot(rmse_ann,model_nodes,label='RMSE')
# # ax26.plot(arv_ann,model_nodes,label='ARV')
# ax26.set(ylabel='ERRORs', xlabel='number of hidden nodes',title='multiple neurons with errors')
# ax26.get_xaxis().get_major_formatter().set_useOffset(False)
# ax26.get_yaxis().get_major_formatter().set_useOffset(False)
# ax26.legend(loc='upper right')

## undo comment to here *** 


#plot MAE error relation
fig24 = plt.figure(24)
ax24 = fig24.add_axes([.11,.11,0.8,0.8])
ax24.plot(step_time,error_data,label=error_text_list[plot_error])
ax24.plot(step_time,error_data_2,label=error_text_list[plot_error_2])
ax24.set(ylabel='ERROR', xlabel='time step',title='ERROR DIAGRAM')
ax24.get_xaxis().get_major_formatter().set_useOffset(False)
ax24.get_yaxis().get_major_formatter().set_useOffset(False)
ax24.legend(loc='upper right')

#plot NEW FUNCTION OUTPUT
fig11 = plt.figure(11)
ax11 = fig11.add_axes([.11,.11,0.8,0.8])
ax11.plot(NEW_FUNCTION_predicted[0:300],'--b',label='availabilities')
ax11.set(ylim=(min(NEW_FUNCTION_predicted)-.000000001,max(NEW_FUNCTION_predicted)+.000000001), ylabel='availability', title='NEW FUNCTION')
ax11.get_xaxis().get_major_formatter().set_useOffset(False)
ax11.get_yaxis().get_major_formatter().set_useOffset(False)
ax11.set_xticklabels([]) # hide X axis
ax11.legend(loc='upper right')

#plot ALL MERTICS
fig12 = plt.figure(12)
ax12 = fig12.add_axes([.11,.11,0.8,0.8])
ax12.plot(d1[0:30],label='availabilities')
ax12.plot(d2[0:30],label='ElectroMogration')
ax12.plot(d3[0:30],label='HCI')
ax12.plot(d4[0:30],label='NBTI')
ax12.plot(d5[0:30],label='TDDB')
ax12.plot(d6[0:30],label='TC')
ax12.set(title='ALL METRICS')
ax12.get_xaxis().get_major_formatter().set_useOffset(False)
ax12.get_yaxis().get_major_formatter().set_useOffset(False)
ax12.set_xticklabels([]) # hide X axis
ax12.legend(loc='upper right')

#compare model results to orginal data
fig2 = plt.figure(2)
ax2 = fig2.add_axes([.11,.11,0.8,0.8])
ax2.plot(Temps_test,y1_predict,'--r',label='predected data')
ax2.plot(Temps_test,y1_test,'-b',label='real data')
ax2.set(ylabel='availability', xlabel='temperature in Kelvin',title='orginal relation VS model relation')
ax2.get_xaxis().get_major_formatter().set_useOffset(False)
ax2.get_yaxis().get_major_formatter().set_useOffset(False)
ax2.legend(loc='upper right')

#compare LR VS SVM models
fig20 = plt.figure(20)
ax20 = fig20.add_axes([.11,.11,0.8,0.8])
ax20.plot(y1_predict[0:500],'--r',label='Linear Regression')
ax20.plot(y1_predict_svm[0:500],'--b',label='SVR')
ax20.set(ylabel='availability',title='Compare all algorithms')
ax20.get_xaxis().get_major_formatter().set_useOffset(False)
ax20.get_yaxis().get_major_formatter().set_useOffset(False)
ax20.set_xticklabels([]) # hide X axis
ax20.legend(loc='upper right')

#Plot all Series
#to plot two lines (training + test) connected we do the following
train_len_int = len(training_data_prediction) #get the length of training examples
train_len = np.arange(0,train_len_int) #create a numpy array in range of it's length
test_len_int = max(train_len)+1 #the value of the first point of the second line (test line)
test_len = np.arange(test_len_int,len(actual_data)) #create a numpy array in range the first point and the last point of the entire dataset

fig21 = plt.figure(21)
ax21 = fig21.add_axes([.11,.11,0.8,0.8])
ax21.plot(actual_data,'-r',label='actual data')
ax21.plot(train_len, training_data_prediction,'--b',label='predicted (training)')
ax21.plot(test_len, y1_predict,'--g',label='predicted (testing)')
ax21.set(ylabel='availability',title='ALL DATA')
ax21.get_xaxis().get_major_formatter().set_useOffset(False)
ax21.get_yaxis().get_major_formatter().set_useOffset(False)
ax21.set_xticklabels([]) # hide X axis
ax21.legend(loc='upper right')

#plot real VS predicted model results
#edit before drawing for scaling
predect_dr = y1_predict[0:50]
real_dr = y1_test[0: 50]

fig22 = plt.figure(22)
ax22 = fig22.add_axes([.11,.11,0.8,0.8])
ax22.plot(predect_dr,'--r',label='predected data')
ax22.plot(real_dr,'-b',label='real data')
# use ylim=(min(availabilities)-.00000001,max(availabilities)+.00000001) to control y axis scale by increasing or decresing zeros
ax22.set(ylim=(min(availabilities)-.000000001,max(availabilities)+.000000001),ylabel='availability', title='real VS predected')
ax22.get_xaxis().get_major_formatter().set_useOffset(False)
ax22.get_yaxis().get_major_formatter().set_useOffset(False)
ax22.legend(loc='upper right')

plt.show()
