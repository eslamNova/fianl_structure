import numpy as np

new_data_1 = []
new_data_2 = []
cpu_c = 8
mem_c = 9
sto_c = 10 
temp_c = 12

#step function
def step_fun(data, key, itr, step):
    step_data = []
    if key == 8:
        data_name = 'cpu_usage'
    elif key == 9:
        data_name = 'mem_usage'
    elif key == 10:
        data_name = 'sto_usage'
    elif key == 11:
        data_name = 'time'
    elif key == 12:
        data_name = 'temperature'
    print("**** step fun *****")
    for i in range(itr):
        data[:,key:key+1] += step
        new_data_1.append(np.array(data[:,key:key+1]))
        step_data.append(np.array(data))
    step_data = np.array(step_data)
    return step_data, new_data_1, data_name

def complete_step_data(data_sample, data_row, rows):
    part_data = np.repeat(data_row, rows, axis=0)
    complete_data = np.append(data_sample, part_data, axis=0) # merge
    print("complete_step_data")
    return complete_data


#test cpu data
def test_cpu_data(data_row, cpu_data, mem_data, sto_data, temperature_data):
    data_name = 'cpu_usage , mem_usage , sto_usage , temperature_usage' 
    for a,b,c,d in zip(cpu_data, mem_data, sto_data, temperature_data):
        data_row[:, cpu_c:cpu_c+1] = a
        data_row[:, mem_c:mem_c+1] = b
        data_row[:, sto_c:sto_c+1] = c
        data_row[:, temp_c:temp_c+1] = d
        new_data_2.append(np.array(data_row))
    new_data2 = np.array(new_data_2)
    return new_data2, data_name