import math
import random
import numpy as np
import pandas as pd

#macros
#temperature
Tr = 295
I =  6
V = 1 
alpha = 0.1
Cp = 0.1*10**-6
h = 50 
As = 60*10**-4
m = 50*10**-3
C = 900
# t = 400 #t can be set to be a changable random value   # t = random.randint(300,10001) 
#availability
MTTFtpo = 27800
MTTR = 8
Ea = 0.642
k = 0.0000863
TPo = 293.15
#Electromigration
Aem = 1  # scale factor
n = 1.1 # - for copper #random***
J = 1*math.exp(6) #the current density - for copper #random***
#HCI
A_HCI = 1  #scale factor
Isub =  1*10**-6    #Substrate current
m_HCI =  3   #factor depending on Substrate current #random***
Ea_HCI = -0.15 #activation energy for HCI #random***
#NBTI
A_NBTI = 1   #constant
Vg = -2  #stress voltage #random***
n_NBTI =  2  #voltage acceleration factor
#TDDB
Ao = 1 #scale factor
y = 3 #field acceleration parameter #random***
Eox = -4 #external electric field
#TC 
A_TC = 1 #CONSTANT
q = 4 #Coffin-Manson exponent #random***
Tnom = 293


#initializing
Temps_K = []
availabilities = []
MTTFems = []
MTTF_HCIs = []
MTTF_NBTIs = []
MTTF_TDDBs =[]
MTTF_TCs = []

def calc_all_metrics(array_m):
	Lentgh = len(array_m)
	for cpur,time,i in zip(array_m[: , 8:9],array_m[: , 11:12],range(0,Lentgh)):
		TP = Tr + (((I*V)+(alpha*alpha*V**2*cpur))/h*As)*(1- math.exp(-(((h*As*3600*abs(time))/(m*C))))) #variable is cpur
        #availability
		MTTFtp = MTTFtpo * math.exp((Ea/k)*(1/TP-1/TPo)) #variable is TP
		availability = MTTFtp / (MTTFtp+MTTR) #variable is MTTFtpo
		availabilities.append(availability) #save
        #EM
		MTTF_EM = Aem * J**-n * math.exp(Ea/(k*TP)) #variables are TP, n, J
		MTTFems.append(MTTF_EM) #save
        #HCI
		MTTF_HCI = A_HCI * Isub**-m_HCI * math.exp(Ea_HCI/(k*TP)) #variables are TP, m_HCI, Ea_HCI
		MTTF_HCIs.append(MTTF_HCI) #save
        #NBTI
		MTTF_NBTI = A_NBTI * Vg**n_NBTI * math.exp(Ea/(k*TP)) #variable is TP
		MTTF_NBTIs.append(MTTF_NBTI) #save
        #TDDB
		MTTF_TDDB = Ao * math.exp(-y*Eox) * math.exp(Ea/(k*TP)) #variables are TP, y
		MTTF_TDDBs.append(MTTF_TDDB) #save
        #TC
		MTTF_TC = A_TC * (Tnom/TP)**-q #variables are TP, q
		MTTF_TCs.append(MTTF_TC) #save

	return availabilities, MTTFems, MTTF_HCIs, MTTF_NBTIs, MTTF_TDDBs, MTTF_TCs
