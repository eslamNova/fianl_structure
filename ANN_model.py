from matplotlib import pyplot as plt
import csv
import math
import random
from numpy import loadtxt
import numpy as np
from sklearn.metrics import mean_squared_error, r2_score
from keras.models import Sequential
from keras.layers import Dense
from sklearn.preprocessing import StandardScaler
from sklearn.cross_validation import train_test_split
from sklearn.model_selection import GridSearchCV
from keras.wrappers.scikit_learn import KerasRegressor
import pandas as pd

def ANN_model_fun(x1_train , y1_train , optimizer, activation, layers, neurons):
	print("1- ANN Regression")

	#model initializing
	model = Sequential()
	model.add(Dense(neurons, input_dim=x1_train.shape[1], activation=activation))
	for i in range(1,layers-1):
		model.add(Dense(neurons, activation =activation))
	
	model.add(Dense(1, activation = activation))
	model.compile(loss='mean_squared_error', optimizer=optimizer, metrics=['accuracy'])
	# model.fit(x1_train, y1_train, epochs=50, batch_size=10, verbose=2)

	# y1_predict= model.predict(x1_test)

	return model