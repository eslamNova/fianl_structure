from sklearn.svm import SVR
from sklearn.svm import LinearSVR
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.cross_validation import train_test_split

def SVM_model_fun(x1_train, y1_train, x1_test):
	svr = LinearSVR(random_state=0)
	svr.fit(x1_train, y1_train)
	y1_predict = svr.predict(x1_test)
	return y1_predict, svr 
